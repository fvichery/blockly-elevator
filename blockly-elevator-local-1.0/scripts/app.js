'use strict';

/**
 * @ngdoc overview
 * @name blocklyElevatorApp
 * @description
 * # blocklyElevatorApp
 *
 * Main module of the application.
 */
angular
  .module('blocklyElevatorApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'scrollGlue',
    'angularFileUpload'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/elevator', {
        templateUrl: 'views/elevator.html',
        controller: 'ElevatorCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
