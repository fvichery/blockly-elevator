'use strict';

angular.module('blocklyElevatorApp')
	.factory('RandomUser', function(){
	
	//constructor
	var RandomUser = function(maxFloor){
		this.initialFloor = Math.floor(Math.random() * (maxFloor+1));
		var availableFloors = [];
		for(var i = 0; i < maxFloor+1 ; i++){
			if(i != this.initialFloor){
				availableFloors.push(i);
			}
		}
		this.floorToGo = availableFloors[Math.floor(Math.random()*availableFloors.length)];
		this.direction = (this.initialFloor-this.floorToGo < 0) ? "UP" : "DOWN";
	};

	//dummy method
	RandomUser.prototype.dummyMethod = function(first_argument) {
		// body...
	};

	return RandomUser;
});
