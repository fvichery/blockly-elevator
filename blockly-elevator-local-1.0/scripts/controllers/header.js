'use strict';

/**
 * @ngdoc function
 * @name blocklyElevatorApp.controller:HeaderCtrl
 * @description
 * # HeaderCtrl
 * Controller of the blocklyElevatorApp
 */
angular.module('blocklyElevatorApp')
  .controller('HeaderCtrl', function ($scope,$location) {
  	$scope.isActive = function (viewLocation) { 
        return viewLocation === $location.path();
    };
  });