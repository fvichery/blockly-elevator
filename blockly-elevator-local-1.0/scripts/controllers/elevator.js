'use strict';

/**
 * @ngdoc function
 * @name blocklyElevatorApp.controller:ElevatorCtrl
 * @description
 * # ElevatorCtrl
 * Controller of the blocklyElevatorApp
 */
angular.module('blocklyElevatorApp')
	.controller('ElevatorCtrl', ['$scope','$timeout','$upload', 'RandomUser', function($scope,$timeout,$upload, RandomUser){

    //Init
    $scope.score = 0;
    $scope.console = "";
    $scope.inProgress = false;
    $scope.evaluationMode = false;
    $scope.countDown = 0;
    $scope.activeNav = 'Blocks';
    $scope.jsCode = '';

    /**
     * Add log in console.
     * @param  {string} text : message to write
     * @param  {string} level : ERROR | WARNING | SUCCESS | INFO(default)
     */
    $scope.appendConsole = function(text,level){
    	var colorClass = "";
    	if( typeof(level) == 'undefined' ){
	        level = "DEBUG";
	    }
	    if( level == 'INFO' ){
	        colorClass = "text-info";
	    }
    	if(level=="ERROR"){
    		colorClass = "text-danger";
    	}
    	if(level=="SUCCESS"){
    		colorClass = "text-success";
    	}
    	if(level=="WARNING"){
    		colorClass = "text-warning";
    	}
    	$scope.console += "> <span class='" + colorClass +"'> ["+ level +"] " + text + "</span><br/>";
    }

    //Elevator var
    var maxFloor = 5;
	var maxUsersInElevator = 5;

    var initWaitingUsers = function(){
    	for(var i = 0; i<maxFloor+1; i++){
    		$scope.waitingUsers[i] = [];
    	}
    }
    $scope.waitingUsers = [];
    initWaitingUsers();
	$scope.usersInElevator = [];
	$scope.currentFloor = 0;
	
	var door = "CLOSE";

	$scope.checkElevatorFloor = function(floor){
		if(floor==$scope.currentFloor){
			return "success";
		}else{
			return "disabled";
		}
	}

	$scope.nbInElevator = function(floor){
		if(floor==$scope.currentFloor){
			return $scope.usersInElevator.length;
		}else{
			return " ";
		}
	}

    ///Blocky code
    Blockly.JavaScript.addReservedWords('code');

   $scope.blocklyLoaded = function(blockly) {
        // Called once Blockly is fully loaded.
        window.Blockly = blockly;
        $scope.jsCode = Blockly.JavaScript.workspaceToCode();
    }

    $scope.getJsBlocky = function(){
    	return Blockly.JavaScript.workspaceToCode();
    }

    $scope.launchBlocklyCode = function(evaluation){
    	$scope.evaluationMode = false;
    	if(evaluation === true){
    		$scope.evaluationMode = true;
    	}

    	$scope.inProgress = true;

    	var elevatorTick = 1000;

    	if($scope.evaluationMode){
    		//speed up
			elevatorTick = 100;
    	}

    	//Reset Elevator
    	$scope.score = 0;
    	$scope.console = "";
    	$scope.waitingUsers = [];
	    initWaitingUsers();
		$scope.usersInElevator = [];
		$scope.currentFloor = 0;
		door = "CLOSE";

    	window.LoopTrap = 1000;
		Blockly.JavaScript.INFINITE_LOOP_TRAP = 'if(--window.LoopTrap == 0) throw "Infinite loop.";\n';
		var code = Blockly.JavaScript.workspaceToCode();
		try {
		  
		  var elevator = new Evaluator(code);
		  //Elevator Code
		  //
		  $scope.countDown = 1000/elevatorTick*10;
		  
		  // Add 1 new user every 4 ticks
		  var newUser = function(){
		  	var user = new RandomUser(maxFloor);
		  	$scope.waitingUsers[user.initialFloor].push(user);
		  	//call the elevator
		  	$scope.appendConsole("New user call : floor = "+user.initialFloor+", direction = "+user.direction+".");
		  	$scope.score -= 1;
		  	elevator.callElevator(user.initialFloor,user.direction);
		  }

		  //Call nextCommand everty ticks
		  var processNextCommand = function(){
	  		var option = elevator.nextCommand();
	  		switch(option){
	  			case "CLOSE" :
	  				if(door=="CLOSE"){
	  					$scope.appendConsole("NextCommand = "+option+", Doors are aleady closed !","ERROR");
	  					// -5 points / error
	  					$scope.score -= 1;
	  				}else{
	  					door = "CLOSE";
	  					$scope.appendConsole("NextCommand = "+option+", Doors are now CLOSED !","INFO");
	  				}
	  				break;
	  			case "OPEN" :
	  				if(door=="OPEN"){
	  					$scope.appendConsole("NextCommand = "+option+", Doors are aleady opened !","ERROR");
	  					// -5 points / error
	  					$scope.score -= 1;
	  				}else{
	  					door = "OPEN";
	  					$scope.appendConsole("NextCommand = "+option+", Doors are now OPENED !","INFO");
	  					//Let users exit the elevator
	  					for(var i=0; i< $scope.usersInElevator.length; i++){
	  						if($scope.usersInElevator[i].floorToGo == $scope.currentFloor){
	  							$scope.usersInElevator.splice(i,1);
	  							//call userHasExited
	  							elevator.userHasExited();
	  							//10 points / userExited
	  							$scope.score += 2;
	  						}
	  					}
	  					//Let users enter the elevator
	  					var availableCapacity = maxUsersInElevator-$scope.usersInElevator.length;
	  					var electedUsers = $scope.waitingUsers[$scope.currentFloor].splice(0,availableCapacity);
	  					//TODO
	  					//mettre les user dans l'ascenseur
	  					for(var i=0; i<electedUsers.length; i++){
	  						$scope.usersInElevator.push(electedUsers[i]);
	  						//call userHasEnterred
	  						elevator.userHasEntered();
	  						//GoToFloor
	  						$scope.appendConsole("User want to go to : floor = "+electedUsers[i].floorToGo+".");
	  						elevator.goToFloor(electedUsers[i].floorToGo);
	  						//10 points / userEntered
	  						$scope.score += 1;
	  					}
	  					//Check full size and warn : elevator is full !
	  					if($scope.usersInElevator.length==maxUsersInElevator){
	  						$scope.appendConsole("Elevator is full !","WARNING");
	  					}
	  					
	  				}
	  				break;
	  			case "UP" :
	  				if($scope.currentFloor!=maxFloor){
	  					$scope.currentFloor++;
	  					$scope.appendConsole("NextCommand = "+option+", Elevator go to floor "+$scope.currentFloor+".","INFO");
	  				}else{
	  					$scope.appendConsole("NextCommand = "+option+", Elevator can't go higher !","ERROR");
	  					// -5 points / error
	  					$scope.score -= 1;
	  				}
	  				break;
	  			case "DOWN" :
	  				if($scope.currentFloor!=0){
	  					$scope.currentFloor--;
	  					$scope.appendConsole("NextCommand = "+option+", Elevator go to floor "+$scope.currentFloor+".","INFO");
	  				}else{
	  					$scope.appendConsole("NextCommand = "+option+", Elevator can't go lower !","ERROR");
	  					// -5 points / error
	  					$scope.score -= 1;
	  				}
	  				break;
	  			default :
	  				$scope.appendConsole("NextCommand = "+option+", Doing nothing.","WARNING");
	  				break;
		  	}
		  }

		  var timer = function(){
	  		$scope.countDown--;
	  		if($scope.countDown<=0){
	  			$scope.inProgress = false;
	  			$scope.appendConsole("Your score : "+$scope.score+" points !","SUCCESS");
	  			$scope.evaluationMode = false;
	  		}
	  	  }

	  	  var clockUser = 3;
	  	  var allFunctions = function(){
	  	  	if($scope.evaluationMode){
		  		timer();
		  	}
		  	if($scope.inProgress){
		  		if(clockUser == 3){
		  			newUser();
		  			clockUser = 1;
		  		}else{
		  			clockUser++;
		  		}
		  		
		  		processNextCommand();
		  		$timeout(allFunctions,elevatorTick);
		  	}
	  	  }

	  	  allFunctions();

		} catch (e) {
		  $scope.appendConsole(e,"ERROR");
		}
    }

    $scope.save_blocks = function(filename) {
        var xml = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
        var data = Blockly.Xml.domToText(xml);
        filesave(data, 'text/xml', filename);
    }

    /**
     * Load blocks from local file.
     */
    function handleFileSelect(evt) {
            var files = evt.target.files; // FileList object

            // Only allow uploading one file.
            if (files.length != 1) {
                return;
            }

            // FileReader
            var reader = new FileReader();
            reader.onloadend = function(event) {
                var target = event.target;
                // 2 == FileReader.DONE
                if (target.readyState == 2) {
                try {
                    var xml = Blockly.Xml.textToDom(target.result);
                } catch (e) {
                    alert('Error parsing XML:\n' + e);
                    return;
                }
                var previous_dom = Blockly.Xml.workspaceToDom(Blockly.mainWorkspace);
                var current_count = Blockly.mainWorkspace.getAllBlocks().length;
                if (current_count && confirm('Replace existing blocks?\n"Cancel" will merge.')) {
                    Blockly.mainWorkspace.clear();
                }
                Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, xml);
                var remaining = Blockly.mainWorkspace.remainingCapacity();
                var maxi = Blockly.maxBlocks;
                if (remaining < 0) {
                    alert("Trop de blocs (%1 > %2).".replace('%1', maxi - remaining)
                                                .replace('%2', maxi));
                    Blockly.mainWorkspace.clear();
                    Blockly.Xml.domToWorkspace(Blockly.mainWorkspace, previous_dom);
                }
                }
                // Reset value of input after loading because Chrome will not fire
                // a 'change' event if the same file is loaded again.
                // TODO fix it !
                document.getElementById('load').value = '';
            };
            reader.readAsText(files[0]);
    }
    document.getElementById('files').addEventListener('change', handleFileSelect, false);

    // Taken from:
    // https://github.com/gasolin/BlocklyDuino/blob/master/blockly/apps/blocklyduino/blockly_helper.js
    // 2013/3/8
    function filesave(data, type, filename) {
        var blob = new Blob([data], {type: type});
        saveAs(blob, filename);
    }



}]);
