'use strict';

/**
 * @ngdoc function
 * @name blocklyElevatorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the blocklyElevatorApp
 */
angular.module('blocklyElevatorApp')
  .controller('MainCtrl', function ($scope,$timeout) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];

  });
